<?php
/**
 * Created by PhpStorm.
 * User: Tran Dinh Duong
 * Date: 12/13/2016
 * Time: 9:46 AM
 */
return [
    /*Product*/
    // Product's statuses
    'STATUS_PRODUCT_OK' => 1,
    'STATUS_PRODUCT_WAITING' => 2,
    // End customer gender
    'GENDER_MALE' => 1,
    'GENDER_FEMALE' => 2,
    'GENDER_BOTH' => 3,
    // Translate's status
    'STATUS_TRANSLATE_OK' => 1,
    'STATUS_TRANSLATE_WAITING_TRANSLATE' => 2,
    'STATUS_TRANSLATE_NOT_YET_REQUEST' => 3,
    // Support status
    'STATUS_SUPPORT_OK' => 1,
    'STATUS_SUPPORT_WAITING' => 2,
    // Attack status
    'STATUS_BID' => 1,
    'STATUS_SUGGESTSENT' => 2,
    'STATUS_NOREPLY' => 3,
    'STATUS_PROCCESS' => 4,
    'STATUS_NOREPONSE' => 5,
    'STATUS_SAMPLE_CHARGE' => 6,
    'STATUS_SAMPLE_FREE' => 7,
    'STATUS_ORDER' => 8,
    'STATUS_REPEAT' => 9,
    // Mail attach type
    'TYPE_ATTACHMENT' => 1,
    'TYPE_PRICE_LIST' => 2,
    'MAIL_CC' => 'buyer_support@couxujapan.net',
    'DOMAIN_SUPPLIER_TEST' => 'https://jpsupplier.test.commerce-sourcing.com',
    'DOMAIN_SUPPLIER_PRODUCT' => 'https://jpsupplier.commerce-sourcing.com',
    'DOMAIN_ADMIN_TEST' => 'https://adsupport.test.commerce-sourcing.com',
    'DOMAIN_ADMIN_PRODUCT' => 'https://adsupport.commerce-sourcing.com',
    /* an_tnh_sc_94_sc_161_start */
    'FREE_DEFAULT_SUPPLIER_ID' => 579,
    /* an_tnh_sc_94_sc_161_end */
];