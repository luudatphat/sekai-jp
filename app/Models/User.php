<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /*Create table relationships*/
    public function mtb_role() {
        return $this->belongsTo('App\Models\Mtb_Role');
    }
    public function supplier(){
        return $this->hasOne('App\Models\Supplier');
    }
}
