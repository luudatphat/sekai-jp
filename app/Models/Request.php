<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Request extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function buyer (){
        return $this->belongsTo('App\Models\Buyer');
    }

    public function mtb_genre (){
        return $this->belongsTo('App\Models\Mtb_Genre');
    }

    public function attacks() {
        return $this->hasMany('App\Models\Attack');
    }
}
