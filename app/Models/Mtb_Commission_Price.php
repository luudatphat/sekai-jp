<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Commission_Price extends Model
{
    protected $table = 'mtb_commission_prices';
    /*Create table relationships*/
    public function suppliers(){
        return $this->hasMany('App\Models\Supplier');
    }
}
