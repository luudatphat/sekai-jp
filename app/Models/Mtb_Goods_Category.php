<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Goods_Category extends Model
{
    protected $table = 'mtb_goods_categories';
}
