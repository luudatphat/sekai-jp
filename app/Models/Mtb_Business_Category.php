<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Business_Category extends Model
{
    protected $table = 'mtb_business_categories';
}
