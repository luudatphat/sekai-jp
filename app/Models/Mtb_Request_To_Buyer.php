<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Request_To_Buyer extends Model
{
    protected $table = 'mtb_request_to_buyers';
}
