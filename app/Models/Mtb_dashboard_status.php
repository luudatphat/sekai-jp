<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mtb_dashboard_status extends Model
{
    use SoftDeletes;
    protected $table = 'mtb_dashboard_statuses';
    protected $dates = ['deleted_at'];
}
