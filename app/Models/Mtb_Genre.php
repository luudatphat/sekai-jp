<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Genre extends Model
{
    protected $table = 'mtb_genres';

    public function supplier_available_categories(){
        return $this->hasMany('App\Models\Supplier_available_category');
    }
    public function contract_report_products(){
        return $this->hasMany('App\Models\Contract_Report_Product');
    }
}
