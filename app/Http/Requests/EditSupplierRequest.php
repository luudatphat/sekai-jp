<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditSupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtRepresentativeEmailAddress' => 'email|max:255',
            'txtContactEmailAddress' => 'email|one_byte|max:255',
            'txtContactGmailAddress' => 'email|one_byte|max:255',
            'txtCompanyEmailAddress' => 'email|one_byte|max:255',
            'txtCompanyWebsite' => 'url|one_byte|max:255',
            'txtRepresentativeMobileNumber' => 'one_byte|max:255',
            'txtContactNameKana' => 'kana|max:255',
            'txtContactMobileNumber' => 'one_byte|max:255',
            'txtContactSkypeID' => 'one_byte|max:255',
            'txtContactLineID' => 'one_byte|max:255',
            'txtContactWechatID' => 'one_byte|max:255',
            'txtContactWhatsappID' => 'one_byte|max:255',
            'txtContactFacebook' => 'one_byte|max:255',
            'cbl_genres' => 'array',
        ];
    }
}
