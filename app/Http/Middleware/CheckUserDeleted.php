<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUserDeleted
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check())
        {
            if (Auth::User()->deleted_at != '')
            {
                Auth::logout();
                return redirect()->to('/login')->with('warning', '入力内容が誤っています');
            }
        }
        return $next($request);
    }
}
