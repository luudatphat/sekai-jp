<?php

namespace App\Http\Controllers\free;

use App\Models\Attack;
use App\Models\Delivery_Request;
use App\Models\MailTemplate;
use App\Models\Memo;
use App\Models\Mtb_Attack_Status;
use App\Models\Price_List;
use App\Models\SuplierEmailAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Redirect;

class FreeBuyerController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    public function buyer_detail($attack_id){
        $_free_user = Auth::user()->free_user;
        if ( $_free_user != '1' ) {
            return Redirect::to( '/buyer_detail/' . $attack_id );
        }
        $data['free_user'] = $_free_user;
        $attack = Attack::find($attack_id);
        if (empty($attack)) {
            abort(404);
        }
        /*check permission*/
        if ($attack->supplier_id != Auth::user()->supplier_id) {
            // abort(403);
        }
        $buyer = $attack->request->buyer;
        $supplier_id = config('constants.FREE_DEFAULT_SUPPLIER_ID');
        $data['attack'] = $attack;
        $data['buyer']  = $buyer;
        $data['mail_templates'] = MailTemplate::where('supplier_id',$supplier_id)->get();
        $data['price_lists'] = Price_List::where('supplier_id',$supplier_id)->get();
        $data['memo_list'] = Memo::where('attack_id',$attack_id)->orderBy('id', 'desc')->get();
        $data['mtb_attack_statuses'] = Mtb_Attack_Status::all();
        $data['sender_list'] = SuplierEmailAddress::where('supplier_id',$supplier_id)->get();
        return view('free.buyer.detail', $data);
    }
}
