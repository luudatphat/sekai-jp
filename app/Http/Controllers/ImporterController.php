<?php

namespace App\Http\Controllers;

use App\Models\Mtb_Commission_Price;
use App\Models\Mtb_Create_Customer_Management_Page_Status;
use App\Models\Mtb_Customer_Stage;
use App\Models\Mtb_Member_Type;
use App\Models\Supplier;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Buyer;
use App\Models\Buyer_Business_Category;
use App\Models\Country;
use App\Models\Mtb_Sns_Tool;
use App\Models\Mtb_Title;
use Illuminate\Support\Facades\DB;

class ImporterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /*function importer buyer info and buyer business categories*/
    public function importer_buyer()
    {
        setlocale(LC_ALL, 'ja_JP.UTF-8');
        $handle = fopen(resource_path() . "/views/import/Buyer_20170410T123648+0900.csv", 'r');
        $j = 0;$i=1;
        $data = [];$email_array = [];
        echo "<pre>";
        while (($row = fgetcsv($handle)) !== false) {
            $array = [];
            $j++;
            if ($j == 1) {
                continue;
            }
            $country = Country::where('country_name_jp', $this->convertToCSVData($row[41]))->get(); // => 36
            $array['country'] = count($country) > 0 ? $country[0]->id : 246;
            $array['address'] = trim($this->convertToCSVData($row[27])); // => 27
            $array['mtb_title_id'] = $this->convertToCSVData($row[30]); // => 3
            $array['name'] = trim($this->convertToCSVData($row[31])); // => 29
            $array['surname'] = trim($this->convertToCSVData($row[32])); // => 28
            $array['company_name'] = trim($this->convertToCSVData($row[42]));// = 37
            $array['company_website_url'] = trim($this->convertToCSVData($row[25])); // => 25
            $sns_tool_id = Mtb_Sns_Tool::where('content', $this->convertToCSVData($row[44]))->get(); // => 5
            $array['mtb_sns_tool_id'] = count($sns_tool_id) > 0 ? $sns_tool_id[0]->id : 7;
            $array['sns_id'] = trim($this->convertToCSVData($row[5])); // => 4
            $array['company_skype_id'] = trim($this->convertToCSVData($row[43])); // => 1
            $email = trim($this->convertToCSVData($row[28])); // => 26
            if(in_array($email , $email_array)){
                $email_array[]  = "temp".$i."_".$email;
                $array['email_address'] = "temp".$i."_".$email;
                $i++;
            }
            else{
                $email_array[]  = $email;
                $array['email_address'] = $email;
            }
            $array['phone_number'] = trim($this->convertToCSVData($row[23])); // => 23
            $array['cell_phone_number'] = trim($this->convertToCSVData($row[24])); // => 22
            $array['purchasing_country'] = trim($this->convertToCSVData($row[18])); // => 19
            $array['sale_country'] = trim($this->convertToCSVData($row[19])); // => 18
            $array['purchase_reason'] = trim($this->convertToCSVData($row[7])); // => 8
            $array['company_strength'] = trim($this->convertToCSVData($row[6])); // => 6
            $array['desired_product'] = trim($this->convertToCSVData($row[26])); // => 24
            $array['status'] = 1;
            $array['rating'] = mb_strlen($this->convertToCSVData($row[40])); // => 35
            $array['created_at'] = date("Y-m-d H:i:s" ,strtotime(trim($this->convertToCSVData($row[36])))); // => 30
            $array['updated_at'] = date("Y-m-d H:i:s" ,strtotime(trim($this->convertToCSVData($row[34])))); // => 31
            $array['year_month'] = date("Y" ,strtotime(trim($this->convertToCSVData($row[36]))))*100+date("m" ,strtotime(trim($this->convertToCSVData($row[36]))));
            $array['Trader'] = trim($this->convertToCSVData($row[9])); // => 10
            $array['Wholesaler'] = trim($this->convertToCSVData($row[10])); // => 11
            $array['Retailer'] = trim($this->convertToCSVData($row[11])); // => 12
            $array['EC_site'] = trim($this->convertToCSVData($row[12])); // => 13
            $array['Company'] = trim($this->convertToCSVData($row[13])); // => 14
            $array['Consumer'] = trim($this->convertToCSVData($row[14])); // => 15
            $array['Manufacturer'] = trim($this->convertToCSVData($row[15])); // => 16
            $array['Other'] = trim($this->convertToCSVData($row[16])); // => 9
            $data[] = $array;
        }
//        die(var_dump($data));
        fclose($handle);
        /*insert database*/
        DB::beginTransaction();
        foreach ($data as $key => $value) {
            $buyer_current = Buyer::where('email_address' , $value['email_address'])->get();
            if(count($buyer_current)>0){
                continue;
            }
            $buyer_new = new Buyer();
            $buyer_new->country = $value['country'];
            $buyer_new->address = $value['address'];
            $buyer_new->mtb_title_id = $value['mtb_title_id'];
            $buyer_new->name = $value['name'];
            $buyer_new->surname = $value['surname'];
            $buyer_new->company_name = $value['company_name'];
            $buyer_new->company_website_url = $value['company_website_url'];
            $buyer_new->mtb_sns_tool_id = $value['mtb_sns_tool_id'];
            $buyer_new->sns_id = $value['sns_id'];
            $buyer_new->company_skype_id = $value['company_skype_id'];
            $buyer_new->email_address = $value['email_address'];
            $buyer_new->phone_number = $value['phone_number'];
            $buyer_new->cell_phone_number = $value['cell_phone_number'];
            $buyer_new->purchasing_country = $value['purchasing_country'];
            $buyer_new->sale_country = $value['sale_country'];
            $buyer_new->purchase_reason = $value['purchase_reason'];
            $buyer_new->company_strength = $value['company_strength'];
            $buyer_new->desired_product = $value['desired_product'];
            $buyer_new->status = $value['status'];
            $buyer_new->rating = $value['rating'];
            $buyer_new->year_month = $value['year_month'];
            $buyer_new->save();
            if (!empty($value['Trader']) && $value['Trader'] == "1") {
                $buyer_business_categories = new Buyer_Business_Category();
                $buyer_business_categories->buyer_id = $buyer_new->id;
                $buyer_business_categories->mtb_business_category_id = 1;
                $buyer_business_categories->save();
            }
            if (!empty($value['Wholesaler']) && $value['Wholesaler'] == "1") {
                $buyer_business_categories = new Buyer_Business_Category();
                $buyer_business_categories->buyer_id = $buyer_new->id;
                $buyer_business_categories->mtb_business_category_id = 2;
                $buyer_business_categories->save();
            }
            if (!empty($value['Retailer']) && $value['Retailer'] == "1") {
                $buyer_business_categories = new Buyer_Business_Category();
                $buyer_business_categories->buyer_id = $buyer_new->id;
                $buyer_business_categories->mtb_business_category_id = 3;
                $buyer_business_categories->save();
            }
            if (!empty($value['EC_site']) && $value['EC_site'] == "1") {
                $buyer_business_categories = new Buyer_Business_Category();
                $buyer_business_categories->buyer_id = $buyer_new->id;
                $buyer_business_categories->mtb_business_category_id = 4;
                $buyer_business_categories->save();
            }
            if (!empty($value['Company']) && $value['Company'] == "1") {
                $buyer_business_categories = new Buyer_Business_Category();
                $buyer_business_categories->buyer_id = $buyer_new->id;
                $buyer_business_categories->mtb_business_category_id = 5;
                $buyer_business_categories->save();
            }
            if (!empty($value['Consumer']) && $value['Consumer'] == "1") {
                $buyer_business_categories = new Buyer_Business_Category();
                $buyer_business_categories->buyer_id = $buyer_new->id;
                $buyer_business_categories->mtb_business_category_id = 6;
                $buyer_business_categories->save();
            }
            if (!empty($value['Manufacturer']) && $value['Manufacturer'] == "1") {
                $buyer_business_categories = new Buyer_Business_Category();
                $buyer_business_categories->buyer_id = $buyer_new->id;
                $buyer_business_categories->mtb_business_category_id = 7;
                $buyer_business_categories->save();
            }
            if (!empty($value['Other']) && $value['Other'] == "1") {
                $buyer_business_categories = new Buyer_Business_Category();
                $buyer_business_categories->buyer_id = $buyer_new->id;
                $buyer_business_categories->mtb_business_category_id = 8;
                $buyer_business_categories->save();
            }
        }
        DB::commit();
        /*end insert database*/
        return view('import.buyer_import');
    }
    /*end function importer buyer info and buyer business categories*/

    /*function importer supplier info and buyer business categories*/
    public function importer_supplier()
    {
        setlocale(LC_ALL, 'ja_JP.UTF-8');
        $handle = fopen(resource_path() . "/views/import/Supliers_20170411.csv", 'r');
        $j = 0;
        $data = [];
        while (($row = fgetcsv($handle)) !== false) {
            $array = [];
            $j++;
            if ($j == 1) {
                continue;
            }
            $array['company_name_jp'] = trim($this->convertToCSVData($row[63])); // => 64
            $array['address'] = trim($this->convertToCSVData($row[42])); // => 15
            $array['company_phone_number'] = trim($this->convertToCSVData($row[50])); // => 58
            $array['company_email_address'] = trim($this->convertToCSVData($row[48])); // => 57
            $array['company_website'] = trim($this->convertToCSVData($row[49])); // => 59
            $array['representative_name_kana'] = trim($this->convertToCSVData($row[44])); // => 13
            $array['representative_name_kanji'] = trim($this->convertToCSVData($row[45])); // => 12
            $array['representative_email_address'] = trim($this->convertToCSVData($row[41])); // => 14
            $array['representative_mobile_number'] = trim($this->convertToCSVData($row[43])); // => 11
            $array['contact_name_kanji'] = trim($this->convertToCSVData($row[52])); // => 10
            $array['contact_name_kana'] = trim($this->convertToCSVData($row[40])); // => 60
            $array['contact_email_address'] = trim($this->convertToCSVData($row[38]));// => 9
            $array['contact_mobile_number'] = trim($this->convertToCSVData($row[39])); // => 8
            $array['contact_skype_id'] = trim($this->convertToCSVData($row[36]));
            $array['contact_line_id'] = trim($this->convertToCSVData($row[37]));
            $array['contact_wechat_id'] = trim($this->convertToCSVData($row[34]));
            $array['contact_whatsapp_id'] = trim($this->convertToCSVData($row[35]));
            $array['contract_start_date'] = date("Y-m-d H:i:s",strtotime(trim($this->convertToCSVData($row[66])))); // 契約開始月
            $array['contract_end_date'] = date("Y-m-d H:i:s",strtotime(trim($this->convertToCSVData($row[68])))); // 契約終了月
            $str_customer_stage = trim($this->convertToCSVData($row[62]));
            $str_customer_stage = empty(trim($str_customer_stage)) ? 1 : trim($str_customer_stage);
            if (!is_numeric($str_customer_stage)) {
                $mtb_customer_stage_id = Mtb_Customer_Stage::where('content', $str_customer_stage)->get();
                $mtb_customer_stage_id = count($mtb_customer_stage_id) > 0 ? $mtb_customer_stage_id[0]->id : 1;
            } else {
                $mtb_customer_stage_id = $str_customer_stage;
            }
            $array['mtb_customer_stage_id'] = $mtb_customer_stage_id;
            $str_member_type = trim($this->convertToCSVData($row[61]));
            $mtb_member_type_id = Mtb_Member_Type::where('content', $str_member_type)->get();
            $mtb_member_type_id = count($mtb_member_type_id) > 0 ? $mtb_member_type_id[0]->id : 5;
            $array['mtb_member_type_id'] = $mtb_member_type_id;
            $array['transaction_price_percent'] = trim($this->convertToCSVData($row[69])); // 取引手数料率（数字確定ぼ場合）
            $str_commission_price = trim($this->convertToCSVData($row[60])); // 成果報酬金額
            if (!empty($str_commission_price)) {
                $mtb_commission_price_id = Mtb_Commission_Price::where('content', $str_commission_price)->get();
                $mtb_commission_price_id = count($mtb_commission_price_id) > 0 ? $mtb_commission_price_id[0]->id : null;
            } else {
                $mtb_commission_price_id = null;
            }
            $array['mtb_commission_price_id'] = $mtb_commission_price_id;
            $array['sale_condition'] = trim($this->convertToCSVData($row[59]));
            $str_create_customer_management_page = trim($this->convertToCSVData($row[64]));
            if (!empty($str_create_customer_management_page)) {
                $mtb_create_customer_management_page_status_id = Mtb_Create_Customer_Management_Page_Status::where('content', $str_create_customer_management_page)->get();
                $mtb_create_customer_management_page_status_id = count($mtb_create_customer_management_page_status_id) > 0 ? $mtb_create_customer_management_page_status_id[0]->id : null;
            } else {
                $mtb_create_customer_management_page_status_id = null;
            }
            $array['mtb_create_customer_management_page_status_id'] = $mtb_create_customer_management_page_status_id;
            $array['company_note'] = trim($this->convertToCSVData($row[8]));
            $array['company_strength'] = trim($this->convertToCSVData($row[10]));
            $array['created_at'] = date("Y-m-d H:i:s" ,strtotime(trim($this->convertToCSVData($row[51]))));
            $array['updated_at'] = date("Y-m-d H:i:s" ,strtotime(trim($this->convertToCSVData($row[55]))));
            $array['support_first_name'] = trim($this->convertToCSVData($row[57])); // サポート担当者
            $data[] = $array;
        }
        fclose($handle);
        /*insert database*/
        DB::beginTransaction();
        $data_login = [];
        $n_all_user = 100;
//        echo '<pre>';
        foreach ($data as $key => $value) {
            $array = [];$array_support = [];
            $email = $value['company_email_address'];
            if(empty($email)){
                $email = "trieu_hh_su+".$n_all_user."@distance-c.com";
                $n_all_user++;
            }
            $support_first_name = $value['support_first_name'];
            if(empty($support_first_name)){
                $email_support = "trieu_hh_sp+".$n_all_user."@distance-c.com";
                $n_all_user++;
                /*create user support*/
                $user_support_current = User::where('first_name' , "Trieu Distance")->get();
                if(count($user_support_current) >0){
                    $support_id = $user_support_current[0]->id;
                }
                else{
                    $new_user = new User();
                    $new_user->type = 1;
                    $new_user->email = $email_support;
                    $string_pass = str_random(8);
                    $array_support['email']     = $email_support;
                    $array_support['password']  = $string_pass;
                    $new_user->password = bcrypt($string_pass);
                    $new_user->status = 1;
                    $new_user->first_name = "Trieu Distance";
                    $new_user->last_name = "Trieu Distance";
                    $new_user->created_at = $value['created_at'];
                    $new_user->created_at = $value['updated_at'];
                    $new_user->save();
                    $support_id = $new_user->id;
                }
                /*End create user support*/
            }
            else{
                $user_support_current = User::where('first_name' , $support_first_name)->get();
                if(count($user_support_current) >0){
                    $support_id = $user_support_current[0]->id;
                }
                else{
                    $email_support = "trieu_hh+".$n_all_user."@distance-c.com";
                    $n_all_user++;
                    /*create user support*/
                    $new_user = new User();
                    $new_user->type = 1;
                    $new_user->email = $email_support;
                    $string_pass = str_random(8);
                    $array_support['email']     = $email_support;
                    $array_support['password']  = $string_pass;
                    $new_user->password = bcrypt($string_pass);
                    $new_user->status = 1;
                    $new_user->first_name = $support_first_name;
                    $new_user->last_name = $support_first_name;
                    $new_user->created_at = $value['created_at'];
                    $new_user->created_at = $value['updated_at'];
                    $new_user->save();
                    $support_id = $new_user->id;
                    /*End create user support*/
                }
            }
            $user_current = User::where('email' , $email)->get();
            $supplier_current = Supplier::where('company_email_address' , $email)->get();
            if(count($user_current) > 0 || count($supplier_current)> 0){
                continue;
            }
            /*create user supplier*/
            $new_user = new User();
            $new_user->type = 2;
            $new_user->email = $email;
            $string_pass = str_random(8);
            $array['email'] = $email;
            $array['password'] = $string_pass;
            $new_user->password = bcrypt($string_pass);
            $new_user->status = 1;
            $new_user->created_at = $value['created_at'];
            $new_user->created_at = $value['updated_at'];
            $new_user->save();
            /*End create user supplier*/
            $new_supplier = new Supplier();
            $new_supplier->user_id  = $new_user->id;
            $new_supplier->company_name_jp  = $value['company_name_jp'];
            $new_supplier->address  = $value['address'];
            $new_supplier->company_phone_number  = $value['company_phone_number'];
            $new_supplier->company_email_address  = $email;
            $new_supplier->company_website  = $value['company_website'];
            $new_supplier->representative_name_kanji  = $value['representative_name_kanji'];
            $new_supplier->representative_name_kana  = $value['representative_name_kana'];
            $new_supplier->representative_email_address  = $value['representative_email_address'];
            $new_supplier->representative_mobile_number  = $value['representative_mobile_number'];
            $new_supplier->contact_name_kanji  = $value['contact_name_kanji'];
            $new_supplier->contact_name_kana  = $value['contact_name_kana'];
            $new_supplier->contact_email_address  = $value['contact_email_address'];
            $new_supplier->contact_mobile_number  = $value['contact_mobile_number'];
            $new_supplier->contact_skype_id  = $value['contact_skype_id'];
            $new_supplier->contact_line_id  = $value['contact_line_id'];
            $new_supplier->contact_wechat_id  = $value['contact_wechat_id'];
            $new_supplier->contact_whatsapp_id  = $value['contact_whatsapp_id'];
            $new_supplier->contract_start_date  = $value['contract_start_date'];
            $new_supplier->contract_end_date  = $value['contract_end_date'];
            $new_supplier->mtb_customer_stage_id  = $value['mtb_customer_stage_id'];
            $new_supplier->mtb_member_type_id  = $value['mtb_member_type_id'];
            $new_supplier->transaction_price_percent  = $value['transaction_price_percent'];
            $new_supplier->mtb_commission_price_id  = $value['mtb_commission_price_id'];
            $new_supplier->sale_condition  = $value['sale_condition'];
            $new_supplier->mtb_create_customer_management_page_status_id  = $value['mtb_create_customer_management_page_status_id'];
            $new_supplier->company_note  = $value['company_note'];
            $new_supplier->company_strength  = $value['company_strength'];
            $new_supplier->created_at = $value['created_at'];
            $new_supplier->created_at = $value['updated_at'];
            $new_supplier->supporter_id = $support_id;
            $new_supplier->save();
            $data_login[]  = $array;
            $data_login[]  = $array_support;
        }
        DB::commit();
//        die(var_dump($data_login));
        if(count($data_login) > 0){
            $response['posts'] = $data_login;
            $fp = fopen(resource_path() . '/views/import/results.json', 'w');
            fwrite($fp, json_encode($response));
            fclose($fp);
        }
        /*end insert database*/
        return view('import.supplier_import');
    }
    /*end function supplier buyer info and buyer business categories*/
    public function update_kana_contact(){
        setlocale(LC_ALL, 'ja_JP.UTF-8');
        $handle = fopen(resource_path() . "/views/import/supplier.csv", 'r');
        $data_kana = [];
//        echo "<pre>";
        DB::beginTransaction();
        while (($row = fgetcsv($handle)) !== false) {
            $array = [];
            $kana_wrong = trim($this->convertToCSVData($row[52])); // current
            $kana_right = trim($this->convertToCSVData($row[40])); // right
            if(!empty($kana_wrong)){
                $update_supplier = Supplier::where('contact_name_kana' , $kana_wrong)->get();
                if(count($update_supplier) >0){
                    $update_supplier->first()->contact_name_kana = $kana_right;
                    $update_supplier->first()->contact_name_kanji = $kana_wrong;
                    $update_supplier->first()->save();
                }
            }
            else{
                if(!empty($kana_right)){
                    $update_supplier = Supplier::where('contact_name_kanji' , $kana_right)->get();
                    if(count($update_supplier) >0) {
                        $update_supplier->first()->contact_name_kana = $kana_right;
                        $update_supplier->first()->contact_name_kanji = $kana_wrong;
                        $update_supplier->first()->save();
                    }
                }
            }
        }
        DB::commit();
        die(var_dump($data_kana));
    }
    /*convert csv japanese character*/
    function convertToCSVData($data)
    {
        return mb_convert_encoding($data, 'UTF-8', 'ASCII,JIS,UTF-8,eucJP-win,SJIS-win');
    }
    /*end convert csv japanese character*/
}
