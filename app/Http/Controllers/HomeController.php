<?php

namespace App\Http\Controllers;

use App\Models\Attack;
use App\Models\Country;
use App\Models\Dashboard_Row;
use App\Models\DashboardBuyerMaster;
use App\Models\DashboardSupplierMaster;
use App\Models\Mtb_Genre;
use App\Models\MtbNewsCategory;
use App\Models\News;
use App\Models\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
/* an_tnh_sc_94_start */
use Redirect;
/* an_tnh_sc_94_end */

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    public function index()
    {
        return redirect('/news');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == '1' ) {
            return Redirect::to( 'free/home' );
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */

        $supplier_id = Auth::user()->supplier->id;

        $attacks = Attack::where('supplier_id',$supplier_id)->get();


        $arr_status_count_by_country = [];
        $arr_status_count = [];
        $arr_status_count['suggest'] = 0;
        $arr_status_count['response_yes'] = 0;
        $arr_status_count['response_no'] = 0;
        $arr_status_count['send_example_free'] = 0;
        $arr_status_count['send_example_charge'] = 0;
        $arr_status_count['order'] = 0;
        $arr_status_count['repeat'] = 0;
        foreach ($attacks as $attack) {
            if(!empty($attack->request)) {
                $dashboard = DashboardBuyerMaster::where([
                    ['supplier_id', '=', $supplier_id],
                    ['request_id', '=', $attack->request->id]
                ])->get()->toArray();
                if (!empty($dashboard)) {
                    //Count number of each status in $dashboard
                    $dashboard_count = array_count_values((array_column($dashboard, 'mtb_attack_status_id')));
                    //Count status of each country
                    /* an_tnh_sc_136_start */
                    if (is_object( $attack->request->buyer ) && isset($arr_status_count_by_country[$attack->request->buyer->country])) {
                    /* an_tnh_sc_136_end */   
                        if (isset($dashboard_count[config('constants.STATUS_SUGGESTSENT')]) && $dashboard_count[config('constants.STATUS_SUGGESTSENT')] > 0) {
                            $arr_status_count_by_country[$attack->request->buyer->country]['suggest'] += $dashboard_count[config('constants.STATUS_SUGGESTSENT')];
                            $arr_status_count_by_country[$attack->request->buyer->country]['response_no'] += (isset($dashboard_count[config('constants.STATUS_NOREPONSE')]) ? $dashboard_count[config('constants.STATUS_NOREPONSE')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['response_yes'] += (isset($dashboard_count[config('constants.STATUS_PROCCESS')]) ? $dashboard_count[config('constants.STATUS_PROCCESS')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['send_example_charge'] += (isset($dashboard_count[config('constants.STATUS_SAMPLE_CHARGE')]) ? $dashboard_count[config('constants.STATUS_SAMPLE_CHARGE')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['send_example_free'] += (isset($dashboard_count[config('constants.STATUS_SAMPLE_FREE')]) ? $dashboard_count[config('constants.STATUS_SAMPLE_FREE')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['order'] += (isset($dashboard_count[config('constants.STATUS_ORDER')]) ? $dashboard_count[config('constants.STATUS_ORDER')] : 0);
                            $arr_status_count_by_country[$attack->request->buyer->country]['repeat'] += (isset($dashboard_count[config('constants.STATUS_REPEAT')]) ? $dashboard_count[config('constants.STATUS_REPEAT')] : 0);
                        }
                    } else {
                        if (isset($dashboard_count[config('constants.STATUS_SUGGESTSENT')]) && $dashboard_count[config('constants.STATUS_SUGGESTSENT')] > 0) {
                            /* an_tnh_sc_136_start */
                            if ( is_object( $attack->request->buyer ) ) {   
                                $arr_status_count_by_country[$attack->request->buyer->country]['suggest'] = $dashboard_count[config('constants.STATUS_SUGGESTSENT')];
                                $arr_status_count_by_country[$attack->request->buyer->country]['response_no'] = (isset($dashboard_count[config('constants.STATUS_NOREPONSE')]) ? $dashboard_count[config('constants.STATUS_NOREPONSE')] : 0);
                                $arr_status_count_by_country[$attack->request->buyer->country]['response_yes'] = (isset($dashboard_count[config('constants.STATUS_PROCCESS')]) ? $dashboard_count[config('constants.STATUS_PROCCESS')] : 0);
                                $arr_status_count_by_country[$attack->request->buyer->country]['send_example_charge'] = (isset($dashboard_count[config('constants.STATUS_SAMPLE_CHARGE')]) ? $dashboard_count[config('constants.STATUS_SAMPLE_CHARGE')] : 0);
                                $arr_status_count_by_country[$attack->request->buyer->country]['send_example_free'] = (isset($dashboard_count[config('constants.STATUS_SAMPLE_FREE')]) ? $dashboard_count[config('constants.STATUS_SAMPLE_FREE')] : 0);
                                $arr_status_count_by_country[$attack->request->buyer->country]['order'] = (isset($dashboard_count[config('constants.STATUS_ORDER')]) ? $dashboard_count[config('constants.STATUS_ORDER')] : 0);
                                $arr_status_count_by_country[$attack->request->buyer->country]['repeat'] = (isset($dashboard_count[config('constants.STATUS_REPEAT')]) ? $dashboard_count[config('constants.STATUS_REPEAT')] : 0);
                            }
                            /* an_tnh_sc_136_end */
                        }
                    }
                    //Count status
                    $arr_status_count['suggest'] += isset($dashboard_count[config('constants.STATUS_SUGGESTSENT')]) ? $dashboard_count[config('constants.STATUS_SUGGESTSENT')] : 0;
                    $arr_status_count['response_no'] += (isset($dashboard_count[config('constants.STATUS_NOREPONSE')]) ? $dashboard_count[config('constants.STATUS_NOREPONSE')] : 0);
                    $arr_status_count['response_yes'] += (isset($dashboard_count[config('constants.STATUS_PROCCESS')]) ? $dashboard_count[config('constants.STATUS_PROCCESS')] : 0);
                    $arr_status_count['send_example_charge'] += (isset($dashboard_count[config('constants.STATUS_SAMPLE_CHARGE')]) ? $dashboard_count[config('constants.STATUS_SAMPLE_CHARGE')] : 0);
                    $arr_status_count['send_example_free'] += (isset($dashboard_count[config('constants.STATUS_SAMPLE_FREE')]) ? $dashboard_count[config('constants.STATUS_SAMPLE_FREE')] : 0);
                    $arr_status_count['order'] += (isset($dashboard_count[config('constants.STATUS_ORDER')]) ? $dashboard_count[config('constants.STATUS_ORDER')] : 0);
                    $arr_status_count['repeat'] += (isset($dashboard_count[config('constants.STATUS_REPEAT')]) ? $dashboard_count[config('constants.STATUS_REPEAT')] : 0);
                }
            }
        }
        $data['arr_status_count_by_country'] = $arr_status_count_by_country;
        $data['arr_status_count'] = $arr_status_count;
        $data['countries'] = Country::all();

//        $year_month_now = Carbon::now()->format("Ym");
//        $year_month_five_ago = Carbon::now()->subMonths(4)->format('Ym');

        $all_status_of_dashboard_row = Dashboard_Row::where('supplier_id','=',$supplier_id)
//                                                    ->where('request_monthly','<=',$year_month_now)
//                                                    ->where('request_monthly','>=',$year_month_five_ago)
                                                    ->get();
        $total_status_bid = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_bid'));
        $total_status_suggestsent = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_suggestsent'));
        $total_status_noreponse = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_noreponse'));
        $total_status_proccess = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_proccess'));
        $total_status_sample_free = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_sample_free'));
        $total_status_sample_charge = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_sample_charge'));
        $total_status_order = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_order'));
        $total_status_repeat = array_sum(array_column($all_status_of_dashboard_row->toArray(),'status_repeat'));

        $data['arr_graph_tab2']['status_bid'] = $total_status_bid;
        $data['arr_graph_tab2']['status_suggestsent'] = $total_status_suggestsent;
        $data['arr_graph_tab2']['status_noreponse'] = $total_status_noreponse;
        $data['arr_graph_tab2']['status_proccess'] = $total_status_proccess;
        $data['arr_graph_tab2']['status_sample_free'] = $total_status_sample_free;
        $data['arr_graph_tab2']['status_sample_charge'] = $total_status_sample_charge;
        $data['arr_graph_tab2']['status_order'] = $total_status_order;
        $data['arr_graph_tab2']['status_repeat'] = $total_status_repeat;

        $all_record_dashboard_supplier_master = DashboardSupplierMaster::where('supplier_id','=',$supplier_id)->get();
        $total_money_order = array_sum(array_column($all_record_dashboard_supplier_master->toArray(),'money'));
        $total_money_repeat = array_sum(array_column($all_record_dashboard_supplier_master->toArray(),'money_repeat'));
        $data['arr_graph_tab2']['money_order'] = $total_money_order;
        $data['arr_graph_tab2']['money_repeat'] = $total_money_repeat;
        $data['categories']             = Mtb_Genre::all();

        return view('home.home', $data);
    }

    /*Graph 3*/
    public function load_graph_3(Request $request) {
        /*array mapping post data to attack status id*/
        $arr_attack_status_mapping = [
            1 => config('constants.STATUS_BID'),
            2 => config('constants.STATUS_SUGGESTSENT'),
            3 => config('constants.STATUS_PROCCESS'),
            4 => 'send_sample',
            5 => config('constants.STATUS_ORDER'),
            6 => config('constants.STATUS_REPEAT'),
        ];
        /*convert date string to integer*/
        $start_date = strtotime($request->start_date);
        $start_date = (date('Y', $start_date)*100 + date('m', $start_date))*100 + date('d', $start_date);
        $end_date = strtotime($request->end_date);
        $end_date = (date('Y', $end_date)*100 + date('m', $end_date))*100 + date('d', $end_date);
        /*get current status data from DB*/
        $current_status_data = $this->getDashboardBuyerMasterByStatus($start_date, $end_date, $arr_attack_status_mapping[$request->attack_status]);
        /*get count record by year month*/
        $current_status_data_value = [];
        foreach ($current_status_data as $key=>$value){
            $current_status_data_value[] = ['year_month' => substr($value['year_month_day'], 0,6)];
        }
        $current_status_data = array_count_values(array_column($current_status_data_value, 'year_month'));

        /*get previous status data of this current status from DB*/
        if ($request->attack_status != 1) {
            //if current status is ORDER or REPEAT then previous status is PROCESS
            $previous_status = (($request->attack_status == 5) || ($request->attack_status == 6)) ? config('constants.STATUS_PROCCESS') : $arr_attack_status_mapping[$request->attack_status - 1];
            $previous_status_data = $this->getDashboardBuyerMasterByStatus($start_date, $end_date, $previous_status);
            /*get count record by year month*/
            $previous_status_data_value = [];
            foreach ($previous_status_data as $key=>$value){
                $previous_status_data_value[] = ['year_month' => substr($value['year_month_day'], 0,6)];
            }
            $previous_status_data = array_count_values(array_column($previous_status_data_value, 'year_month'));
        }
        $all_supplier_data = $this->getDashboardBuyerMasterByStatus($start_date, $end_date, $arr_attack_status_mapping[$request->attack_status], true);
        /*group by year month*/
        $arr_group_by_year_month = [];
        foreach ($all_supplier_data as $item) {
            $arr_group_by_year_month[substr($item['year_month_day'],0 ,6)][] = $item;
        }
        /*calculate average*/
        $arr_average = [];
        foreach ($arr_group_by_year_month as $year_month => $year_month_item) {
            $count = count($year_month_item);
            $count_supplier = count(array_unique(array_column($year_month_item, 'supplier_id')));
            $arr_average[$year_month] = $count/$count_supplier;
        }
        $response['current_status_data'] = $current_status_data;
        $response['previous_status_data'] = isset($previous_status_data) ? $previous_status_data : [];
        $response['arr_average'] = $arr_average;
        return json_encode($response);
    }

    protected function getDashboardBuyerMasterByStatus ($start_date, $end_date, $attack_status, $flag_get_all_supplier = false) {
        $supplier_id = Auth::user()->supplier->id;
        $where = [
            ['year_month_day', '>=', $start_date],
            ['year_month_day', '<=', $end_date]
        ];
        if (!$flag_get_all_supplier) {
            $where[] = ['supplier_id', '=', $supplier_id];
        }
        $result = DashboardBuyerMaster::where($where);
        if ($attack_status == 'send_sample') {
            $result->where(function ($query) {
                $query->where('mtb_attack_status_id','=', config('constants.STATUS_SAMPLE_FREE'));
                $query->orWhere('mtb_attack_status_id','=', config('constants.STATUS_SAMPLE_CHARGE'));
            });
        } else {
            $result->where('mtb_attack_status_id','=', $attack_status);
        }
        $result->orderBy('year_month','asc');
        return $result->get()->toArray();
    }
    /*
     * function deviation
     *
     * */
    function find_stdev($arr){
        $a=0;
        foreach($arr as $val) {
            $a += (pow($val,2));
        }
        $n = count($arr);
        $sum = array_sum($arr);
        $a = $a*$n;
        $b = pow($sum,2);
        $c = ($a-$b)/($n*($n-1));
        $stdev = sqrt($c);

        return $stdev;
    }
    /*
     * show new info world-connect.com
     *
     *  */
    public function news(){
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == '1' ) {
            return Redirect::to( 'free/news' );
        }
        $result['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */

        /*INCLUDE LIBRARY SIMPLE DOM HTML */
        include(app_path().'/functions/simple_html_dom.php');
        /*END INCLUDE LIBRARY SIMPLE DOM HTML */
        $ch = curl_init("http://world-conect.com/feed");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        $html = curl_exec($ch);
        curl_close($ch);
        $html = str_get_html($html);
        $data  = [];
        if($html){
            foreach ($html->find('item') as $item){
                $array = [];
                $array['title'] = $item->find('title', 0)->plaintext;
                $array['link'] = substr($item->find('comments' , 0)->plaintext , 0, strpos($item->find('comments' , 0)->plaintext , "#"));
                $array['pubDate'] = date('Y/m/d H:i:s' ,strtotime($item->find('pubDate', 0)->plaintext));
                $data[] = $array;
            }
        }
        $result["list_news_world_connect"] = $data;

        /*get categories*/
        $result["list_categories_news"]         = MtbNewsCategory::where('content', '<>' ,'Other')->get();
        $result["list_categories_news_other"]   = MtbNewsCategory::where('content', 'Other')->get();
        /*end get categories*/
        /*get news admin*/
        $list_news = [];
        $list_news_other = [];
        $list_admin_news = News::where('status' , 1)->orderBy('updated_at', 'desc')->get();
        foreach ($list_admin_news as $value){
            $list_news[$value->category_id][] =  $value->toArray();
            if($value->category_id==6){
                $list_news_other[$value->category_id][] = $value->toArray();
            }
        }
        $result["list_news_admin"] = $list_news;
        $result["list_news_other"] = $list_news_other;
        /*end get news admin*/

        return view('news.new' , $result);
    }

    /*
     * view detail news other
     *
     */
    public function details($id){
        $data = [];
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == '1' ) {
            return Redirect::to( '/free/news/details/' . $id );
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */
        
        if(!empty($id)){
            $object = News::find($id);
            if (empty($object)) {
                abort(404);
            }
            $data['detail_news'] = News::where('id' , $id)->where('status' , 1)->get();
        }
        return view('news.details' , $data);
    }

    public function home_4(Request $request){

        if($request->isMethod('post')){

            $suplier_id = Auth::user()->supplier->id;
            $list_supliers = Supplier::whereHas('user', function ($query) {
                $query->where('status',1);
            })->get();

            $num_supliers = count($list_supliers);

            $categoryId = $request->input('categoryId');
            $endDate = $request->input('endDate');
            $startDate = $request->input('startDate');

            $total_bid = 0;
            $total_sugesstion = 0;
            $total_reply = 0;
            $total_sample = 0;
            $total_order = 0;
            $total_repeat = 0;

            $list_total_bid = Array();
            $list_total_sugesstion = Array();
            $list_total_reply = Array();
            $list_total_sample = Array();
            $list_total_order = Array();
            $list_total_repeat = Array();

            $total_bid_sup = 0;
            $total_sugesstion_sup = 0;
            $total_reply_sup = 0;
            $total_sample_sup = 0;
            $total_order_sup = 0;
            $total_repeat_sup = 0;

            $bidding_deviation = 0;
            $suggested_deviation = 0;
            $status_reply_deviation = 0;
            $status_sample_deviation = 0;
            $status_order_deviation = 0;
            $status_repeat_deviation = 0;

            if(empty($categoryId) && empty($endDate) && empty($startDate))
            {
                $result = DashboardBuyerMaster::all();

                foreach ($list_supliers as $suplier){
                    $bid = 0;
                    $sugesstion = 0;
                    $reply = 0;
                    $sample = 0;
                    $order = 0;
                    $repeat = 0;
                    foreach ($result as $item) {
                        if ($item->mtb_attack_status_id == config('constants.STATUS_BID') && $item->supplier_id == $suplier->id) {
                            $bid++;
                        }
                        if ($item->mtb_attack_status_id == config('constants.STATUS_SUGGESTSENT') && $item->supplier_id == $suplier->id ) {
                            $sugesstion++;
                        }
                        if ((($item->mtb_attack_status_id == config('constants.STATUS_NOREPONSE')) || ($item->mtb_attack_status_id == config('constants.STATUS_PROCCESS'))) && ($item->supplier_id == $suplier->id) ) {
                            $reply++;
                        }
                        if ((($item->mtb_attack_status_id == config('constants.STATUS_SAMPLE_FREE')) || ($item->mtb_attack_status_id == config('constants.STATUS_SAMPLE_CHARGE'))) && ($item->supplier_id == $suplier->id) ) {
                            $sample++;
                        }
                        if ($item->mtb_attack_status_id == config('constants.STATUS_ORDER') && $item->supplier_id == $suplier->id ) {
                            $order++;
                        }
                        if ($item->mtb_attack_status_id == config('constants.STATUS_REPEAT') && $item->supplier_id == $suplier->id ) {
                            $repeat++;
                        }
                    }
                    $list_total_bid[$suplier->id] = $bid;
                    $list_total_sugesstion[$suplier->id] = $sugesstion;
                    $list_total_reply[$suplier->id] = $reply;
                    $list_total_sample[$suplier->id] = $sample;
                    $list_total_order[$suplier->id] = $order;
                    $list_total_repeat[$suplier->id] = $repeat;
                }

                foreach ($result as $item) {
                    if ($item->mtb_attack_status_id == config('constants.STATUS_BID')) {
                        $total_bid++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_BID') && $item->supplier_id == $suplier_id ) {
                        $total_bid_sup++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_SUGGESTSENT')) {
                        $total_sugesstion++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_SUGGESTSENT') && $item->supplier_id == $suplier_id ) {
                        $total_sugesstion_sup++;
                    }

                    if (($item->mtb_attack_status_id == config('constants.STATUS_NOREPONSE')) || ($item->mtb_attack_status_id == config('constants.STATUS_PROCCESS'))) {
                        $total_reply++;
                    }

                    if ((($item->mtb_attack_status_id == config('constants.STATUS_NOREPONSE')) || ($item->mtb_attack_status_id == config('constants.STATUS_PROCCESS'))) && ($item->supplier_id == $suplier_id) ) {
                        $total_reply_sup++;
                    }

                    if (($item->mtb_attack_status_id == config('constants.STATUS_SAMPLE_FREE')) || ($item->mtb_attack_status_id == config('constants.STATUS_SAMPLE_CHARGE'))) {
                        $total_sample++;
                    }

                    if ((($item->mtb_attack_status_id == config('constants.STATUS_SAMPLE_FREE')) || ($item->mtb_attack_status_id == config('constants.STATUS_SAMPLE_CHARGE'))) && ($item->supplier_id == $suplier_id) ) {
                        $total_sample_sup++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_ORDER')) {
                        $total_order++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_ORDER') && $item->supplier_id == $suplier_id ) {
                        $total_order_sup++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_REPEAT')) {
                        $total_repeat++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_REPEAT') && $item->supplier_id == $suplier_id ) {
                        $total_repeat_sup++;
                    }
                }

            }else{

                $startDate = str_replace('/', '-', $startDate);
                $startDate = Carbon::parse($startDate);

                $endDate = str_replace('/', '-', $endDate);
                $endDate = Carbon::parse($endDate);

                $query = DashboardBuyerMaster::where('id', '<>', null);

                if(!empty($categoryId)){
                    $query->where('mtb_goods_category_id', '=', $categoryId);
                }

                if(!empty($endDate)){
                    $query->where('year_month_day','<=',$endDate->format('Ymd'));
                }

                if(!empty($startDate)){
                    $query->where('year_month_day','>=',$startDate->format('Ymd'));
                }

                $result = $query->get();

                foreach ($list_supliers as $suplier){
                    $bid = 0;
                    $sugesstion = 0;
                    $reply = 0;
                    $sample = 0;
                    $order = 0;
                    $repeat = 0;
                    foreach ($result as $item) {
                        if ($item->mtb_attack_status_id == config('constants.STATUS_BID') && $item->supplier_id == $suplier->id) {
                            $bid++;
                        }
                        if ($item->mtb_attack_status_id == config('constants.STATUS_SUGGESTSENT') && $item->supplier_id == $suplier->id ) {
                            $sugesstion++;
                        }
                        if ((($item->mtb_attack_status_id == config('constants.STATUS_NOREPONSE')) || ($item->mtb_attack_status_id == config('constants.STATUS_PROCCESS'))) && ($item->supplier_id == $suplier->id) ) {
                            $reply++;
                        }
                        if ((($item->mtb_attack_status_id == config('constants.STATUS_SAMPLE_FREE')) || ($item->mtb_attack_status_id == config('constants.STATUS_SAMPLE_CHARGE'))) && ($item->supplier_id == $suplier->id) ) {
                            $sample++;
                        }
                        if ($item->mtb_attack_status_id == config('constants.STATUS_ORDER') && $item->supplier_id == $suplier->id ) {
                            $order++;
                        }
                        if ($item->mtb_attack_status_id == config('constants.STATUS_REPEAT') && $item->supplier_id == $suplier->id ) {
                            $repeat++;
                        }
                    }

                    $list_total_bid[$suplier->id] = $bid;
                    $list_total_sugesstion[$suplier->id] = $sugesstion;
                    $list_total_reply[$suplier->id] = $reply;
                    $list_total_sample[$suplier->id] = $sample;
                    $list_total_order[$suplier->id] = $order;
                    $list_total_repeat[$suplier->id] = $repeat;
                }

                foreach ($result as $item) {
                    if ($item->mtb_attack_status_id == config('constants.STATUS_BID')) {
                        $total_bid++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_BID') && $item->supplier_id == $suplier_id ) {
                        $total_bid_sup++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_SUGGESTSENT')) {
                        $total_sugesstion++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_SUGGESTSENT') && $item->supplier_id == $suplier_id ) {
                        $total_sugesstion_sup++;
                    }

                    if (($item->mtb_attack_status_id == config('constants.STATUS_NOREPONSE')) || ($item->mtb_attack_status_id == config('constants.STATUS_PROCCESS'))) {
                        $total_reply++;
                    }

                    if ((($item->mtb_attack_status_id == config('constants.STATUS_NOREPONSE')) || ($item->mtb_attack_status_id == config('constants.STATUS_PROCCESS'))) && ($item->supplier_id == $suplier_id) ) {
                        $total_reply_sup++;
                    }

                    if (($item->mtb_attack_status_id == config('constants.STATUS_SAMPLE_FREE')) || ($item->mtb_attack_status_id == config('constants.STATUS_SAMPLE_CHARGE'))) {
                        $total_sample++;
                    }

                    if ((($item->mtb_attack_status_id == config('constants.STATUS_SAMPLE_FREE')) || ($item->mtb_attack_status_id == config('constants.STATUS_SAMPLE_CHARGE'))) && ($item->supplier_id == $suplier_id) ) {
                        $total_sample_sup++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_ORDER')) {
                        $total_order++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_ORDER') && $item->supplier_id == $suplier_id ) {
                        $total_order_sup++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_REPEAT')) {
                        $total_repeat++;
                    }

                    if ($item->mtb_attack_status_id == config('constants.STATUS_REPEAT') && $item->supplier_id == $suplier_id ) {
                        $total_repeat_sup++;
                    }
                }
            }

            if($total_bid > 0){
                $bidding_deviation = ($total_bid_sup -  ($total_bid/$num_supliers)) / $this->find_stdev($list_total_bid)*10 + 50;
            }else{
                $bidding_deviation = 0;
            }

            if($total_sugesstion > 0){
                $suggested_deviation = ($total_sugesstion_sup -  ($total_sugesstion/$num_supliers)) / $this->find_stdev($list_total_sugesstion)*10 + 50;
            }else{
                $suggested_deviation = 0;
            }

            if($total_reply > 0){
                $status_reply_deviation = ($total_reply_sup -  ($total_reply/$num_supliers)) / $this->find_stdev($list_total_reply)*10 + 50;
            }else{
                $status_reply_deviation = 0;
            }

            if($total_sample > 0){
                $status_sample_deviation = ($total_sample_sup -  ($total_sample/$num_supliers)) / $this->find_stdev($list_total_sample)*10 + 50;
            }else{
                $status_sample_deviation = 0;
            }

            if($total_order > 0){
                $status_order_deviation = ($total_order_sup -  ($total_order/$num_supliers)) / $this->find_stdev($list_total_order)*10 + 50;
            }else{
                $status_order_deviation = 0;
            }

            if($total_repeat > 0){
                $status_repeat_deviation = ($total_repeat_sup -  ($total_repeat/$num_supliers)) / $this->find_stdev($list_total_repeat)*10 + 50;
            }else{
                $status_repeat_deviation = 0;
            }

            $count_bid = 0;
            $count_sugesstion = 0;
            $count_reply = 0;
            $count_sample = 0;
            $count_order = 0;
            $count_repeat = 0;

            foreach ($list_total_bid as $bid){
                if($bid > 0){
                    $count_bid++;
                }
            }

            foreach ($list_total_sugesstion as $bid){
                if($bid > 0){
                    $count_sugesstion++;
                }
            }

            foreach ($list_total_reply as $bid){
                if($bid > 0){
                    $count_reply++;
                }
            }

            foreach ($list_total_sample as $bid){
                if($bid > 0){
                    $count_sample++;
                }
            }

            foreach ($list_total_order as $bid){
                if($bid > 0){
                    $count_order++;
                }
            }

            foreach ($list_total_repeat as $bid){
                if($bid > 0){
                    $count_repeat++;
                }
            }

            $array = Array();

            $array['total']['bid'] = ($count_bid > 0)?$total_bid/$count_bid:0;
            $array['total']['sugesstion'] = ($count_sugesstion > 0)?$total_sugesstion/$count_sugesstion:0;
            $array['total']['reply'] = ($count_reply > 0)?$total_reply/$count_reply:0;
            $array['total']['sample'] = ($count_sample > 0)?$total_sample/$count_sample:0;
            $array['total']['order'] = ($count_order > 0)?$total_order/$count_order:0;
            $array['total']['repeat'] = ($count_repeat > 0)?$total_repeat/$count_repeat:0;

            $array['total']['sup_bid'] = $total_bid_sup;
            $array['total']['sup_sugesstion'] = $total_sugesstion_sup;
            $array['total']['sup_reply'] = $total_reply_sup;
            $array['total']['sup_sample'] = $total_sample_sup;
            $array['total']['sup_order'] = $total_order_sup;
            $array['total']['sup_repeat'] = $total_repeat_sup;

            $array['deviation']['bid'] = $bidding_deviation;
            $array['deviation']['sugesstion'] = $suggested_deviation;
            $array['deviation']['reply'] = $status_reply_deviation;
            $array['deviation']['sample'] = $status_sample_deviation;
            $array['deviation']['order'] = $status_order_deviation;
            $array['deviation']['repeat'] = $status_repeat_deviation;

            return json_encode($array);
        }

    }
    /*get data graph 4*/
    public function get_data_graph_handler(Request $request){
        /*get data to supplier to month*/
        $array = [];$array_company = [];
        $user_id = Auth::user()->id;
        $supplier = Supplier::where('user_id' , $user_id)->get();
        $supplier_id = $supplier[0]->id;
        $status = explode(',', $request->status);
        $date = explode('/', $request->from_date);
        $from_date = ($date[0]*100+$date[1])*100+$date[2];
        $date = explode('/', $request-> to_date);
        $to_date = ($date[0]*100+$date[1])*100+$date[2];
        $suppliers = Supplier::whereHas('user', function ($query) {
            $query->where('status',1);
        })->orderBy('company_name_jp')->get();
        if(count($status)>1){
            $data = DashboardBuyerMaster::where('supplier_id', $supplier_id)->where('year_month_day', '>=',$from_date)->where('year_month_day', '<=',$to_date)->where('mtb_attack_status_id' , $status[0])->orWhere('mtb_attack_status_id' , $status[1])->get()->groupBy('year_month_day');
        }
        else{
            $data = DashboardBuyerMaster::where('supplier_id', $supplier_id)->where('year_month_day', '>=',$from_date)->where('year_month_day', '<=',$to_date)->where('mtb_attack_status_id' , $status[0])->get()->groupBy('year_month_day');
        }
        if(count($status)>1) {
            $dt = DashboardBuyerMaster::where('year_month_day', '>=',$from_date)->where('year_month_day', '<=',$to_date)->where('mtb_attack_status_id' , $status[0])->orWhere('mtb_attack_status_id' , $status[1])->get()->groupBy('year_month_day');
            $count_company = DashboardBuyerMaster::where('year_month_day', '>=',$from_date)->where('year_month_day', '<=',$to_date)->where('mtb_attack_status_id' , $status[0])->orWhere('mtb_attack_status_id' , $status[1])->get();
        }
        else{
            $dt = DashboardBuyerMaster::where('year_month_day', '>=',$from_date)->where('year_month_day', '<=',$to_date)->where('mtb_attack_status_id' , $status[0])->get()->groupBy('year_month_day');
            $count_company = DashboardBuyerMaster::where('year_month_day', '>=',$from_date)->where('year_month_day', '<=',$to_date)->where('mtb_attack_status_id' , $status[0])->get();
        }
        $count_company = $count_company->toArray();
        foreach ($count_company as $key => $value){
            $year_month = substr($value['year_month_day'], 0, strlen($value['year_month_day'])-2);
            if(isset($array_company[$year_month])&&in_array($value['supplier_id'] ,$array_company[$year_month])){
                continue;
            }
            $array_company[$year_month][] = $value['supplier_id'];
        }
        $key_array = [];
        $data_item = [];$dt_item = [];
        foreach ($data as $key => $value){
            $key_current = substr($key, 0, strlen($key)-2);
            if(count($key_array)>0&&in_array($key_current, $key_array)){
                foreach ($value as $item){
                    $data_item[$key_current][] = $item->toArray();
                }
                continue;
            }
            foreach ($value as $item){
                $data_item[$key_current][] = $item->toArray();
            }
            $key_array[] = $key_current;
        }
        $key_array = [];
        foreach ($dt as $key => $value){
            $key_current = substr($key, 0, strlen($key)-2);
            if(count($key_array)>0&&in_array($key_current, $key_array)){
                foreach ($value as $item){
                    $dt_item[$key_current][] = $item->toArray();
                }
                continue;
            }
            foreach ($value as $item){
                $dt_item[$key_current][] = $item->toArray();
            }
            $key_array[] = $key_current;
        }
        $date_from = explode("/", $request->from_date);
        $date_to = explode("/", $request->to_date);
        $date_from = $date_to[2]<$date_from[2]?$date_from[0]."/".$date_from[1]."/".$date_to[2]:$date_from[0]."/".$date_from[1]."/".$date_from[2];
        for($i= $date_from ; $i<=$request->to_date;$i=date('Y/m/d',strtotime("+1 month", strtotime($i)))){
            $date = explode('/',$i);
            $date_month = ($date[0]*100+$date[1]);
            $array[$date_month]['date_month'] = $date[1]."月".$date[0]."年";
            if(count($data_item) >0 && isset($data_item[$date_month])){
                $array[$date_month]['company_current'] = count($data_item[$date_month]);
            }
            else{
                $array[$date_month]['company_current'] = 0;
            }
            if(count($dt_item) >0 && count($array_company) > 0 && isset($dt_item[$date_month]) && isset($array_company[$date_month])){
                $array[$date_month]['averagre_supplier'] = count($dt_item[$date_month])/count($array_company[$date_month]);
            }
            else{
                $array[$date_month]['averagre_supplier'] = 0;
            }
        }
        /*end get data to supplier to month*/

        /*calculator deviation with status */
        /*count supplier with status*/
        $array_list = [];
        foreach ($count_company as $key => $value){
            $year_month = substr($value['year_month_day'], 0, strlen($value['year_month_day'])-2);
            $array_list[$year_month][$value['supplier_id']][] = 1;
        }
        foreach ($array_list as $key => $item){
            foreach ($item as $key1 => $value){
                $array_list[$key][$key1] = count($value);
            }
        }
        /*end count supplier with status*/
        for($i= $date_from ; $i<=$request->to_date;$i=date('Y/m/d',strtotime("+1 month", strtotime($i)))){
            $date = explode('/',$i);
            $date_month = ($date[0]*100+$date[1]);
            if(isset($array_list[$date_month])) {
                $array_sup = $array_list[$date_month];
                foreach ($suppliers as $supplier){
                    if(!isset($array_sup[$supplier->id])){
                        $array_list[$date_month][$supplier->id] = 0;
                    }
                }
            }
            else{
                foreach ($suppliers as $supplier){
                    $array_list[$date_month][$supplier->id] = 0;
                }
            }
        }
        for($i= $date_from ; $i<=$request->to_date;$i=date('Y/m/d',strtotime("+1 month", strtotime($i)))){
            $date = explode('/',$i);
            $date_month = ($date[0]*100+$date[1]);
            if(isset($array_list[$date_month])){
                $list_array_status_month = $array_list[$date_month];
                if(isset($list_array_status_month[$supplier_id])){
                    if(collect($list_array_status_month)->sum() > 0){
                          $array[$date_month]['deviation'] = ($list_array_status_month[$supplier_id] -  collect($list_array_status_month)->avg()) / $this->find_stdev($list_array_status_month)*10 + 50;
                    }else{
                        $array[$date_month]['deviation'] = 0.0;
                    }
                }
                else{
                    $array[$date_month]['deviation'] = 0.0;
                }

            }
            else{
                $array[$date_month]['deviation'] = 0.0;
            }
        }
        /*end calculator deviation with status*/
        echo json_encode($array);
    }
}
