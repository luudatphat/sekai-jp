<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'attack_id' => 'required|numeric',
            'invoice' => 'required|max:5120',
            'packing_list' => 'max:5120',
            'number_contact' => 'required|numeric',

            'product_name_1' => 'required',
            'product_category_1' => 'required',
            'price_1' => 'required|numeric',
            'quantity_1' => 'required|numeric',
            'couxu_commission_1' => 'required|numeric',

            'product_name_2' => 'required_with:product_category_2,price_2,quantity_2,couxu_commission_2',
            'product_category_2' => 'required_with:product_name_2,price_2,quantity_2,couxu_commission_2',
            'price_2' => 'required_with:product_category_2,product_name_2,quantity_2,couxu_commission_2|numeric',
            'quantity_2' => 'required_with:product_category_2,price_2,product_name_2,couxu_commission_2|numeric',
            'couxu_commission_2' => 'required_with:product_category_2,price_2,quantity_2,product_name_2|numeric',

            'product_name_3' => 'required_with:product_category_3,price_3,quantity_3,couxu_commission_3',
            'product_category_3' => 'required_with:product_name_3,price_3,quantity_3,couxu_commission_3',
            'price_3' => 'required_with:product_category_3,product_name_3,quantity_3,couxu_commission_3|numeric',
            'quantity_3' => 'required_with:product_category_3,price_3,product_name_3,couxu_commission_3|numeric',
            'couxu_commission_3' => 'required_with:product_category_3,price_3,quantity_3,product_name_3|numeric',

            'product_name_4' => 'required_with:product_category_4,price_4,quantity_4,couxu_commission_4',
            'product_category_4' => 'required_with:product_name_4,price_4,quantity_4,couxu_commission_4',
            'price_4' => 'required_with:product_category_4,product_name_4,quantity_4,couxu_commission_4|numeric',
            'quantity_4' => 'required_with:product_category_4,price_4,product_name_4,couxu_commission_4|numeric',
            'couxu_commission_4' => 'required_with:product_category_4,price_4,quantity_4,product_name_4|numeric',

            'product_name_5' => 'required_with:product_category_5,price_5,quantity_5,couxu_commission_5',
            'product_category_5' => 'required_with:product_name_5,price_5,quantity_5,couxu_commission_5',
            'price_5' => 'required_with:product_category_5,product_name_5,quantity_5,couxu_commission_5|numeric',
            'quantity_5' => 'required_with:product_category_5,price_5,product_name_5,couxu_commission_5|numeric',
            'couxu_commission_5' => 'required_with:product_category_5,price_5,quantity_5,product_name_5|numeric',

            'product_name_6' => 'required_with:product_category_6,price_6,quantity_6,couxu_commission_6',
            'product_category_6' => 'required_with:product_name_6,price_6,quantity_6,couxu_commission_6',
            'price_6' => 'required_with:product_category_6,product_name_6,quantity_6,couxu_commission_6|numeric',
            'quantity_6' => 'required_with:product_category_6,price_6,product_name_6,couxu_commission_6|numeric',
            'couxu_commission_6' => 'required_with:product_category_6,price_6,quantity_6,product_name_6|numeric',

            'product_name_7' => 'required_with:product_category_7,price_7,quantity_7,couxu_commission_7',
            'product_category_7' => 'required_with:product_name_7,price_7,quantity_7,couxu_commission_7',
            'price_7' => 'required_with:product_category_7,product_name_7,quantity_7,couxu_commission_7|numeric',
            'quantity_7' => 'required_with:product_category_7,price_7,product_name_7,couxu_commission_7|numeric',
            'couxu_commission_7' => 'required_with:product_category_7,price_7,quantity_7,product_name_7|numeric',
        ];
    }

    public function messages(){
        return [
            'product_name_2.required_with' => trans('validation.required'),
            'product_category_2.required_with' => trans('validation.required'),
            'price_2.required_with' => trans('validation.required'),
            'quantity_2.required_with' => trans('validation.required'),
            'couxu_commission_2.required_with' => trans('validation.required'),
            'product_name_3.required_with' => trans('validation.required'),
            'product_category_3.required_with' => trans('validation.required'),
            'price_3.required_with' => trans('validation.required'),
            'quantity_3.required_with' => trans('validation.required'),
            'couxu_commission_3.required_with' => trans('validation.required'),
            'product_name_4.required_with' => trans('validation.required'),
            'product_category_4.required_with' => trans('validation.required'),
            'price_4.required_with' => trans('validation.required'),
            'quantity_4.required_with' => trans('validation.required'),
            'couxu_commission_4.required_with' => trans('validation.required'),
            'product_name_5.required_with' => trans('validation.required'),
            'product_category_5.required_with' => trans('validation.required'),
            'price_5.required_with' => trans('validation.required'),
            'quantity_5.required_with' => trans('validation.required'),
            'couxu_commission_5.required_with' => trans('validation.required'),
            'product_name_6.required_with' => trans('validation.required'),
            'product_category_6.required_with' => trans('validation.required'),
            'price_6.required_with' => trans('validation.required'),
            'quantity_6.required_with' => trans('validation.required'),
            'couxu_commission_6.required_with' => trans('validation.required'),
            'product_name_7.required_with' => trans('validation.required'),
            'product_category_7.required_with' => trans('validation.required'),
            'price_7.required_with' => trans('validation.required'),
            'quantity_7.required_with' => trans('validation.required'),
            'couxu_commission_7.required_with' => trans('validation.required'),
        ];
    }
}
