@extends('layouts.master')
@section('title','HOME')
@section('sekai_content')
    <table class="table table-bordered">
        <thead>
        <th></th>
        @foreach($requests as $request)
            <th>{{$request->id}}</th>
        @endforeach
        </thead>
        <tbody>
        @foreach($suppliers as $supplier)
        <tr>
            <td>{{$supplier->company_name_jp}}</td>
            @foreach($requests as $request)
            <td>{{$request->id}}</td>
            @endforeach
        </tr>
        @endforeach
        </tbody>

    </table>
@endsection