@extends('layouts.master')
@section('title','詳細メール')
@section('sekai_content')
    <style>
        h2 { margin-top: 10px; border-left: none; }
    </style>
    <section>
        <h2><b>デモの情報を表示しています</b></h2>
        <header><h2>詳細メール</h2></header>
        <article>
            <table class="table-type-01">
                <tr>
                    <th>登録日</th>
                    <td>{{ date_format(date_create($mail->created_at),'Y-m-d') }}</td>
                </tr>
                <tr>
                    <th>レコード番号</th>
                    <td>{{ $rd_number[array_rand($rd_number, 1)] }}</td>
                </tr>
                <tr>
                    <th>企業名</th>
                    <td>{{ $mail->attack->request->buyer->company_name }}</td>
                </tr>
                <tr>
                    <th>担当者名</th>
                    <td>{{ $mail->supplier->contact_name_kanji }}</td>
                </tr>
                <tr>
                    <th>件名</th>
                    <td>COUXU : About your request</td>
                </tr>
                <tr>
                    <th>本文</th>
                    <td>{!!nl2br(e($mail->content))!!}</td>
                </tr>
                <tr>
                    <th>添付ファイル</th>
                    <td>
                        <ul>
                            @foreach ($attachment_lists as $item)
                                @if (is_null($item['deleted_at']))
                                    <li><a href="{{ "/free/mail-download/".$item['id'] }}">{{ $item['file_name'] }}</a></li>
                                @else
                                    <li>{{ $item['file_name'] }}</li>
                                @endif
                            @endforeach
                        </ul>
                    </td>
                </tr>
            </table>
            <div class="text-center">
                <a href="{{url('/free/mail-list')}}" class=""><button type="button" class="button-back">送信済みメール一覧に戻る</button></a>
            </div>
        </article>
    </section>
@endsection