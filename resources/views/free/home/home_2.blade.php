<style>
    g>g>path[fill='#ffffff']{
        fill:black;
    }
    g>text[fill='#000000']{
        fill: #ddaa55;
    }
</style>
{{--<div id="" class="" style="width: 1500px; min-height: 500px">--}}
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Genre', '件数',{ role: 'tooltip' }, '件数',  { role: 'tooltip' }, '件数',  { role: 'tooltip' } ],
                ['入札', {{ $arr_graph_tab2['status_bid'] }}, '入札 : {{ $arr_graph_tab2['status_bid'].' 件' }}', null, '', null, ''],
                ['提案', {{ $arr_graph_tab2['status_suggestsent'] }}, '提案 : {{ $arr_graph_tab2['status_suggestsent'].' 件' }}', null, '', null, ''],
                ['返信', {{ $arr_graph_tab2['status_proccess'] }}, '進展あり返信 : {{ $arr_graph_tab2['status_proccess'].' 件' }}', {{ $arr_graph_tab2['status_noreponse'] }},  '進展なし返信 : {{ $arr_graph_tab2['status_noreponse'].' 件' }}', null, ''],
                ['サンプル', {{ $arr_graph_tab2['status_sample_charge'] }}, '有償サンプル : {{ $arr_graph_tab2['status_sample_charge'].' 件' }}', null, '', {{ $arr_graph_tab2['status_sample_free'] }}
                    , '無償サンプル : {{ $arr_graph_tab2['status_sample_free'].' 件' }}'],
                ['受注', {{ $arr_graph_tab2['status_order'] }}, '受注 : {{ $arr_graph_tab2['status_order'].' 件' }}', null, '', null, ''],
                ['リピート', {{ $arr_graph_tab2['status_repeat'] }}, 'リピート : {{ $arr_graph_tab2['status_repeat'].' 件' }}', null, '', null, '']
            ]);

            var view = new google.visualization.DataView(data);

            var options = {
                backgroundColor: { fill:'transparent'},
                legend: { position: 'none' },
                bar: { groupWidth: '40' },
                isStacked: true,
                colors: ['#ddaa55','#e05b45','#2cb18d'],
                'width':1314,
                'height':532,
                chartArea:{left:0,top:0,width:"100%",height:"100%"},
                hAxis: { textPosition: 'none' },
                vAxis : { textPosition: 'none',gridlines: { color : 'transparent'}},

            };

            var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values_2"));
            function selectHandler() {
                var selectedItem = chart.getSelection()[0];
                if (selectedItem) {
                    var url = location.protocol + "//" + location.host + "/attack_list?status=";
                    if(selectedItem.row == 0 && selectedItem.column == 1){
                        window.open(url+1, '_self');
                    }else if(selectedItem.row == 1 && selectedItem.column == 1){
                        window.open(url+2, '_self');
                    }else if(selectedItem.row == 2 && selectedItem.column == 1){
                        window.open(url+4, '_self');
                    }else if(selectedItem.row == 2 && selectedItem.column == 3){
                        window.open(url+5, '_self');
                    }else if(selectedItem.row == 3 && selectedItem.column == 1){
                        window.open(url+6, '_self');
                    }else if(selectedItem.row == 3 && selectedItem.column == 5){
                        window.open(url+7, '_self');
                    }else if(selectedItem.row == 4 && selectedItem.column == 1){
                        window.open(url+8, '_self');
                    }else if(selectedItem.row == 5 && selectedItem.column == 1){
                        window.open(url+9, '_self');
                    }
                }
            }
            /* an_tnh_sc_94_sc_154_start */
            // google.visualization.events.addListener(chart, 'select', selectHandler);
            /* an_tnh_sc_94_sc_154_start */
            chart.draw(view, options);
        }
    </script>

        <div class="content-charts-01">
            <div class="title-chart-01">累計活動数</div>
            <div id="columnchart_values_2" class="content-chart-01"></div>
            <div class="content-tips-01 clearfix">
                <div class="tips">
                    <div class="tip">入札<span>{{ $arr_graph_tab2['status_bid'] }}<small>件</small></span></div>
                </div>
                <!-- end .tips -->
                <div class="tips">
                    <div class="tip">提案<span>{{ $arr_graph_tab2['status_suggestsent'] }}<small>件</small></span></div>
                </div>
                <!-- end .tips -->
                <div class="tips">
                    <div class="tip">進展あり返信<span>{{ $arr_graph_tab2['status_proccess'] }}<small>件</small></span></div>
                    <div class="tip tip-type-01">進展なし返信<span>{{ $arr_graph_tab2['status_noreponse'] }}<small>件</small></span></div>
                </div>
                <div class="tips">
                    <div class="tip">有償サンプル<span>{{ $arr_graph_tab2['status_sample_charge'] }}<small>件</small></span></div>
                    <div class="tip tip-type-02">無償サンプル<span>{{ $arr_graph_tab2['status_sample_free'] }}<small>件</small></span></div>
                </div>
                <!-- end .tips -->
                <div class="tips">
                    <div class="tip">受注<span>{{ $arr_graph_tab2['status_order'] }}<small>件</small></span></div>
                </div>
                <!-- end .tips -->
                <div class="tips">
                    <div class="tip">リピート<span>{{ $arr_graph_tab2['status_repeat'] }}<small>件</small></span></div>
                </div>
                <!-- end .tips -->
            </div>
        </div>
        <!-- end .content-chart-01 -->
        <div class="content-row-table-01 clearfix">
            <table class="table-col-01">
                <thead>
                <tr>
                    <th>入札</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td data-before="件数" data-after="件"><span>{{ $arr_graph_tab2['status_bid'] }}</span></td>
                </tr>
                </tbody>
            </table>
            <table class="table-col-01">
                <thead>
                <tr>
                    <th>提案</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td data-before="件数" data-after="件"><span>{{ $arr_graph_tab2['status_suggestsent'] }}</span></td>
                </tr>
                <tr>
                    <td data-before="発展率" data-after="％"><span>{{ ($arr_graph_tab2['status_bid'] == 0) ? 0 : round(($arr_graph_tab2['status_suggestsent'] / $arr_graph_tab2['status_bid'] * 100),2) }}</span></td>
                </tr>
                </tbody>
            </table>
            <table class="table-col-02">
                <thead>
                <tr>
                    <th>進展あり返信</th>
                    <th class="bg-head-table-01">進展なし返信</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td data-before="件数" data-after="件"><span>{{ $arr_graph_tab2['status_proccess'] }}</span></td>
                    <td data-before="件数" data-after="件"><span>{{ $arr_graph_tab2['status_noreponse'] }}</span></td>
                </tr>
                <tr>
                    <td data-before="発展率" data-after="％"><span>{{ ($arr_graph_tab2['status_suggestsent'] == 0) ? 0 : round(($arr_graph_tab2['status_proccess'] / $arr_graph_tab2['status_suggestsent'] * 100),2) }}</span></td>
                    <td data-before="発展率" data-after="％"><span>{{ ($arr_graph_tab2['status_suggestsent'] == 0) ? 0 : round(($arr_graph_tab2['status_noreponse'] / $arr_graph_tab2['status_suggestsent'] * 100),2)  }}</span></td>
                </tr>
                </tbody>
            </table>
            <table class="table-col-02">
                <thead>
                <tr>
                    <th>有償サンプル</th>
                    <th class="bg-head-table-02">無償サンプル</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td data-before="件数" data-after="件"><span>{{ $arr_graph_tab2['status_sample_charge'] }}</span></td>
                    <td data-before="件数" data-after="件"><span>{{ $arr_graph_tab2['status_sample_free'] }}</span></td>
                </tr>
                <tr>
                    <td data-before="発展率" data-after="％"><span>{{ ($arr_graph_tab2['status_proccess'] == 0) ? 0 : round(($arr_graph_tab2['status_sample_charge'] / ($arr_graph_tab2['status_proccess']) * 100),2) }}</span></td>
                    <td data-before="発展率" data-after="％"><span>{{ ($arr_graph_tab2['status_proccess'] == 0) ? 0 : round(($arr_graph_tab2['status_sample_free'] / ($arr_graph_tab2['status_proccess']) * 100),2) }}</span></td>
                </tr>
                </tbody>
            </table>
            <table class="table-col-01">
                <thead>
                <tr>
                    <th>受注</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td data-before="件数" data-after="件"><span>{{ $arr_graph_tab2['status_order'] }}</span></td>
                </tr>
                <tr>
                    <td data-before="発展率" data-after="％"><span>{{ ($arr_graph_tab2['status_sample_free'] + $arr_graph_tab2['status_sample_charge'] == 0) ? 0 : round(($arr_graph_tab2['status_order'] / ($arr_graph_tab2['status_sample_free'] + $arr_graph_tab2['status_sample_charge']) * 100),2) }}</span></td>
                </tr>
                <tr>
                    <td data-before="金額/JPY" data-after="￥"><span class="fix-font-number">{{ number_format($arr_graph_tab2['money_order']) }}</span></td>
                </tr>
                </tbody>
            </table>
            <table class="table-col-01">
                <thead>
                <tr>
                    <th>リピート</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td data-before="件数" data-after="件"><span>{{ $arr_graph_tab2['status_repeat'] }}</span></td>
                </tr>
                <tr>
                    <td data-before="発展率" data-after="％"><span>{{ ($arr_graph_tab2['status_order'] == 0) ? 0 : round(($arr_graph_tab2['status_repeat'] / $arr_graph_tab2['status_order'] * 100),2) }}</span></td>
                </tr>
                <tr>
                    <td data-before="金額/JPY" data-after="￥"><span class="fix-font-number">{{ number_format($arr_graph_tab2['money_repeat']) }}</span></td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- end .content-row-table-01 -->
