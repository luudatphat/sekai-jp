<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Title of the document</title>
    <style>
        #index-video {
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            background: #000;
            background-size: cover;
            position: fixed;
            right: 0;
            bottom: 0;
            z-index: -1000;
        }
        img, video, object {
            max-width: 100%;
            height: auto;
            vertical-align: bottom;
            border: none;
        }
        #index {
            width: 100%;
            height: 100%;
            /*background: url(images/dot-3x3.png);*/
            position: relative;
        }
        #index-logo {
            position: absolute;
            top: 50%;
            left: 50%;
            margin: 200px 0 0 -300px;
        }
        .col-login{
            margin-bottom: 20px;
        }
        .col-login>input{
            border-radius: 15px;
        }
    </style>
    <link rel="stylesheet" href="/css/app.css">
</head>

<body>

<video loop="" id="index-video">
    <source src="{{ url('/images/index.mp4') }}" type="video/mp4">
</video>

<div id="index">
    <div class="container">
        <div class="row">
            <div class="form-group" id="index-logo">
                <div class="col-md-6 col-md-offset-3 col-login">
                    <img src="{{ url('/images/index-logo.png') }}" alt="">
                </div>
                <div class="clear-fix"></div>
                <div class="col-md-6 col-md-offset-3 col-login">
                    <input class="form-control">
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 col-md-offset-3 col-login">
                    <input class="form-control">
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 col-md-offset-3 col-login">
                    <input class="form-control">
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>