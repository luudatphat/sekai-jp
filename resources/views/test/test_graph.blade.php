@extends('layouts.master')
@section('title','HOME')
@section('sekai_content')
    <style>
        /* Style tooltip start*/
        /*div.google-visualization-tooltip > ul > li > span {*/
            /*color: #ddaa55 !important;*/
        /*}*/
        /*div.google-visualization-tooltip{*/
            /*background-color: #0c0c0c!important;*/
            /*border-radius: 10px!important;*/
            /*width: 10%!important;*/
        /*}*/
        /*rect{*/
            /*border-radius: 10px!important;*/
        /*}*/


        g>g>path[fill='#ffffff']{
            fill:black;
        }
        g>text[fill='#000000']{
            fill: #ddaa55;
        }
        /* Style tooltip end*/
    </style>
    {{--<script type="text/javascript">--}}
        {{--google.charts.load('current', {packages: ['corechart', 'bar','line']});--}}
        {{--google.charts.setOnLoadCallback(drawMultSeries);--}}

        {{--function drawMultSeries() {--}}
            {{--var data = google.visualization.arrayToDataTable([--}}
                {{--['City', 'Number',{type: 'string', role: 'tooltip'},{ role: 'style' },'Test',{type: 'string', role: 'tooltip'},{'type': 'string', 'role': 'style'}],--}}
                {{--['Row 1', 50, "Row 1 \n$600K" ,'#ddaa55', 48,"123123" ,'point { size: 4; fill-color: #555; }'],--}}
                {{--['Row 2', 40, 'Row 2 \nSunspot','#ddaa55', 38,"456456" ,'point { size: 4; fill-color: #555; }'],--}}
                {{--['Row 3', 20, 'Row 3 \n$800K ','#ddaa55', 18,"789789" ,'point { size: 4; fill-color: #555; }'],--}}
                {{--['Row 4', 30, 'Row 4 \n$1M ', '#e05b45', 28,"101010" ,'point { size: 4; fill-color: #555; }'],--}}
                {{--['Row 5', 10, 'Row 5 \n$600K ','#ddaa55', 15,"111111" ,'point { size: 4; fill-color: #555; }'],--}}
                {{--['Row 6', 20, 'Row 6 \nSunspot ','#ddaa55', 25,"121212" ,'point { size: 4; fill-color: #555; }'],--}}
                {{--['Row 7', 25, 'Row 7 \n$800K ', '#ddaa55', 13,"131313" ,'point { size: 4; fill-color: #555; }'],--}}
                {{--['Row 8', 30, 'Row 8 \n$1M ','#ddaa55', 17,"141414" ,'point { size: 4; fill-color: #555; }'],--}}
            {{--]);--}}

            {{--var options = {--}}
                {{--chartArea: {width: '80%'},--}}
{{--//                tooltip: { isHtml: true },--}}
                {{--series: {1: {type: 'line'}},--}}
                {{--legend: { position: 'none' },--}}
                {{--width: 1500,--}}
                {{--height: 500,--}}
                {{--hAxis: { textPosition: 'none' },--}}
                {{--vAxis : { textPosition: 'none' },--}}
                {{--pointSize: 4,--}}
                {{--lineWidth: 4,--}}
                {{--pointColor : '#555',--}}
                {{--colors: ['#ddaa55','#78ccb5'],--}}
            {{--};--}}

            {{--var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));--}}
            {{--chart.draw(data, options);--}}
        {{--}--}}
    {{--</script>--}}
    {{--<div id="chart_div"></div>--}}
    <script type="text/javascript">
        google.charts.load("current", {packages:['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Name column', 'Orange','Red','Green'],
                ['2010', 30, null, null],
                ['2020', 16, null, null],
                ['2030', 28, 15, null],
                ['2020', 16, null, 22],
                ['2030', 28, null, null]
            ]);
            var view = new google.visualization.DataView(data);
//            var a = linear-gradient('#ffcc66', '#cc9933');
            var options = {
                width: 600,
                height: 400,
                legend: { position: 'top', maxLines: 5 },
//                hAxis: { textPosition: 'none' },
                vAxis : { textPosition: 'none' },
                legend: { position: 'none' },
                bar: { groupWidth: '35%' },
                isStacked: true,
                colors: ['#ffcc66','#cc6633','#339999'],
                width: 1500,
                height: 500,
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
            chart.draw(view, options);
        }
    </script>
    <div id="columnchart_values" style="width: 900px; height: 300px;"></div>
@endsection
{{--<html>--}}
{{--<head>--}}
{{--</head>--}}
{{--<body style="width: 100%; height: 100%;">--}}
    {{--<video autoplay>--}}
        {{--<source src="{{ url('/images/index.mp4') }}">--}}
    {{--</video>--}}
    {{--<div id="index">--}}
        {{--<img src="{{ url('/images/index-logo.png') }}" alt="" id="index-logo">--}}
    {{--</div>--}}
{{--</body>--}}
{{--</html>--}}
{{--<!DOCTYPE html>--}}
{{--<html>--}}
{{--<head>--}}
    {{--<meta charset="UTF-8">--}}
    {{--<title>Title of the document</title>--}}
{{--</head>--}}

{{--<body>--}}
{{--Content of the document......--}}
{{--</body>--}}

{{--</html>--}}