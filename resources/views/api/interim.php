
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/welcart_25hoursbuyers/style.css">
    <title>Interim | 25HOURS BUYERS.com</title>
</head>
<body>
<div id="mainContent">
    <div class="wrap">
        <div id="content">
            <section id="page">

                <h2 id="pageTtl">
                    Your registration request has been received
                </h2>

                <p>
                    A member of our team will contact you to confirm your details.<br>
                    After confirmation, a temporary password will be sent via email to the email address you registered with. Please use it to sign in and update your password.
                </p>

                <p>
                    You can still view the products on our website while you wait for confirmation.<br>
                    Prices, MOQ&#8217;s, and other specific details will be visible once you have signed in.
                </p>

            </section>
        </div>
    </div>
</div>
</body>
</html>