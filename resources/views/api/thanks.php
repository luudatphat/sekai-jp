
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/welcart_25hoursbuyers/style.css">
    <title>Interim | 25HOURS BUYERS.com</title>
</head>
<body>
<div id="mainContent">
    <div class="wrap">
        <div id="content">
            <section id="page">

                <h2 id="pageTtl">
                    Thank you for your Inquiry
                </h2>

                <p>
                    We will soon get back to you after a review. Thank you for your patience.
                </p>

                <p>
                    Inquiry received, thank you very much.<br>
                    On the check, we will contact you shortly content that I had sent.
                </p>
            </section>
        </div>
    </div>
</div>
</body>
</html>