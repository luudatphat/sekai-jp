@extends('layouts.master')
@section('sekai_content')
    <h1 class="h1 bg-primary text-center h1_list_user">サプライヤー企業 詳細情報</h1>
    <div class="container">
        <div class="row row_detail_foreign">
            <div class="col-md-6">
                <label for="id">ID </label>
                <input type="text" class="form-control" name="" value="couxutrading">
            </div>
            <div class="col-md-6">
                <label for="id">PW </label>
                <input type="text" class="form-control" name="" value="asdfghjkl1234567890">
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-6">
                <label for="id">貴社名 (日本語) </label>
                <input type="text" class="form-control" name="" value="COUXU株式会社">
            </div>
            <div class="col-md-6">
                <label for="id">貴社名 (日本語) </label>
                <input type="text" class="form-control" name="" value="COUXU Corporation">
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-2">
                <label for="id">郵便番号 </label>
                <input type="text" class="form-control" name="" value="101-0042">
            </div>
            <div class="col-md-2">
                <label for="id">都道府県 </label>
                <input type="text" class="form-control" name="" value="東京都">
            </div>
            <div class="col-md-8">
                <label for="id">市町村区以下 </label>
                <input type="text" class="form-control" name="" value="千代田区神田東松下町31-1 神田三義ビル4階">
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-3">
                <label for="id">代表電話番号 </label>
                <input type="text" class="form-control" name="" value="03-5298-5000">
            </div>
            <div class="col-md-3">
                <label for="id">代表FAX番号 </label>
                <input type="text" class="form-control" name="" value="03-5298-5000">
            </div>
            <div class="col-md-6">
                <label for="id">代表メールアドレス (ない場合はinfo@couxu.jpを入力)</label>
                <input type="text" class="form-control" name="" value="info@couxu.jp">
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-6">
                <label for="id">コーポレートウェブサイト  </label>
                <input type="text" class="form-control" name="" value="http://couxu.jp">
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-3">
                <label for="id">代表者名 (漢字) </label>
                <input type="text" class="form-control" name="" value="大村 晶彦">
            </div>
            <div class="col-md-3">
                <label for="id">代表者名 (カナ) </label>
                <input type="text" class="form-control" name="" value="オオムラ アキヒコ">
            </div>
            <div class="col-md-3">
                <label for="id">代表者 メールアドレス</label>
                <input type="text" class="form-control" name="" value="a-ohmura@couxu.jp">
            </div>
            <div class="col-md-3">
                <label for="id">代表者 携帯電話番号</label>
                <input type="text" class="form-control" name="" value="080-2009-3657">
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-3">
                <label for="id">ご担当者名 (漢字) </label>
                <input type="text" class="form-control" name="" value="渡辺 勁佑">
            </div>
            <div class="col-md-3">
                <label for="id">ご担当者名 (カナ) </label>
                <input type="text" class="form-control" name="" value="ワタナベ ケイスケ">
            </div>
            <div class="col-md-3">
                <label for="id">ご担当者者 メールアドレス</label>
                <input type="text" class="form-control" name="" value="k-watanabe@couxu.jp">
            </div>
            <div class="col-md-3">
                <label for="id">ご担当者 携帯電話番号</label>
                <input type="text" class="form-control" name="" value="080-2012-8690">
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-3">
                <label for="id">ご担当者 Skype ID</label>
                <input type="text" class="form-control" name="" value="toshiii0405">
            </div>
            <div class="col-md-3">
                <label for="id">ご担当者 LINE ID</label>
                <input type="text" class="form-control" name="" value="080-2012-8690">
            </div>
            <div class="col-md-3">
                <label for="id">ご担当者 WeChat ID</label>
                <input type="text" class="form-control" name="" value="keisuke0405">
            </div>
            <div class="col-md-3">
                <label for="id">ご担当者 WhatsApp ID</label>
                <input type="text" class="form-control" name="" value="81-80-2012-8690">
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-3">
                <label for="id">ご担当者 Google アカウント</label>
                <input type="text" class="form-control" name="" value="oxf0405@gmail.com">
            </div>
            <div class="col-md-3">
                <label for="id">ご担当者 Facebook</label>
                <input type="text" class="form-control" name="" value="keisuke0405">
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-3">
                <label for="id">契約開始月 </label>
                <input type="text" class="form-control" name="" value="2013年11月14日">
            </div>
            <div class="col-md-3">
                <label for="id">契約終了月 </label>
                <input type="text" class="form-control" name="" value="2014年11月13日">
            </div>
            <div class="col-md-3">
                <label for="id">契約期間 </label>
                <input type="text" class="form-control" name="" value="1年間/6ヶ月間/3年間">
            </div>
            <div class="col-md-3">
                <label for="id">契約ステータス </label>
                <input type="text" class="form-control" name="" value="契約期間中/契約期間終了">
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-12">
                <label for="id">顧客ステージ </label>
                <input type="text" class="form-control" name="" value="スタートアップ (初回メール未)/運用ステージ(初回メール済み)/運用ステージ(初回メール済み・価格表済み)/実感ステージ">
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-6">
                <label for="id">案件担当者 </label>
                <input type="text" class="form-control" name="" value="大村/井口/高橋/小沼/渡辺/力也/エラム">
            </div>
            <div class="col-md-6">
                <label for="id">会員内容 </label>
                <input type="text" class="form-control" name="" value="有料会員/無料会員/旧会員/コンサル会員/満了 退会会員">
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-2">
                <label for="id">取引手数料率(数字確定) </label>
                <input type="text" class="form-control" name="" value="00">
            </div>
            <div class="col-md-4">
                <label for="id">成果報酬金額 </label>
                <input type="text" class="form-control" name="" value="５〜１０％/数字確定/イレギュラー">
            </div>
            <div class="col-md-6">
                <label for="id">売買条件 </label>
                <input type="text" class="form-control" name="" value="">
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-4">
                <label for="id">顧客管理ページ作成 </label>
                <input type="text" class="form-control" name="" value="未/済/案内済み">
            </div>
            <div class="col-md-8">
                <label for="id">掲載進捗 </label>
                <input type="text" class="form-control" name="" value="未対応/顧客商品準備中/商品情報回収済/25h登録済/掲載必要ない">
            </div>
        </div>


        <div class="row row_detail_foreign">
            <div class="col-md-12">
                <label for="id">ご対応可能ジャンル (複数選択可) </label>
                <textarea class="form-control" rows="5">□アパレル　□バック・ケース　□時計・ジュエリー・眼鏡　□美容・パーソナルケア　□健康・医療　□スポーツ・娯楽　□家具　□日用雑貨・園芸　□機械部品　□ギフト・工芸品　□食品・飲料　□AV機器・カメラ　□電装品　□証明・灯具　□パソコン・ソフトウェア　□車・バイク　　　　　　　□物流サービス　□製造・加工機械　□建設・不動産　□放送・印刷　□オフィス機器・教育用品　□測定器・分析器　□繊維・皮革　　　　　　　　　□化学薬品・化成品　□治金・鉱物　□靴・日用品　□服飾雑貨　□家電・オーディオ機器　□建材・建築資材　□工具　□玩具・ホビー　　　　　　　□電信・通信　セキュリティ設備　□電子部品　□農業　□店舗用品・設備　□ゴム・プラスチック製品　□エネルギー産業　□環境保護　　　　　　　□ビジネスサービス　□デットストック
                </textarea>
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-12">
                <label for="id">これが得意！！企業の強み </label>
                <textarea class="form-control" rows="5">本番以外ならなんでもおっしゃってください！専属の娘しかいないお店しか紹介しません！

                </textarea>
            </div>
        </div>

        <div class="row row_detail_foreign">
            <div class="col-md-12 text-center">
                <a title="Edit Company" class="btn btn-primary po_a">編集</a>
                <input title="Save Company" type="submit" name="save" class="btn btn-primary" value="保存">
            </div>
        </div>

    </div>
@endsection